const router = require('express').Router();
const paySecurityController = require('../controllers/paysecurity.controller');
const autMiddleware = require('../middlewares/auth.middleware')



// //ruta para traer una persona
router.post('/getpayperson', autMiddleware, paySecurityController.getPayPerson)

module.exports = router