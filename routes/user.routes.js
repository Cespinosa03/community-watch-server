const router = require('express').Router();
const userController = require('../controllers/user.controller');
const autMiddleware = require('../middlewares/auth.middleware')

//ruta traer todos los usuarios
router.get('/getall', autMiddleware, userController.getUser);

module.exports = router;
