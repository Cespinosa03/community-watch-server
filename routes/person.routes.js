const router = require('express').Router();
const personController = require('../controllers/person.controller');
const autMiddleware = require('../middlewares/auth.middleware')


//ruta para crear personas
router.post('/create', personController.createPerson);

//ruta para traer todas las personas
router.get('/getallpersons', personController.getAllPerson)

//ruta para traer una persona
router.post('/getonepersons', autMiddleware, personController.getOnePerson)

//ruta para traer una persona
router.post('/locationperson', personController.locationPerson)

//ruta para traer una persona
router.post('/vehicleperson',  personController.vehiclePerson)


module.exports = router