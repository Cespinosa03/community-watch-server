const { Sequelize, DataTypes } = require("sequelize");
const sequelizeDB = require("../database/db");
const modelUser = require('../models/user.model');

const modelRole = sequelizeDB.define('jdvrole', {
    RoleId: {
        type: DataTypes.INTEGER, 
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    RoleName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    CreatedAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        allowNull: false,
    },
    CreatedBy: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    ModifiedAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
    },
    ModifiedBy: {
        type: DataTypes.STRING
    },
     IsActive: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
    }
},
{
    timestamps: false,
    tableName: "JDVRole"
})


module.exports = modelRole;