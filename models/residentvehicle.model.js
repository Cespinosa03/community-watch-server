const { Sequelize, DataTypes } = require("sequelize");
const sequelizeDB = require("../database/db");

const modelResidentVehicle = sequelizeDB.define('jdvresidentvehicle', {
    ResidentVehicleId: {
        type: DataTypes.INTEGER, 
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    PersonId: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    Type: {
        type: DataTypes.STRING,
    },
    Brand: {
        type: DataTypes.STRING,
    },
    Model: {
        type: DataTypes.STRING
    },
    Color: {
        type: DataTypes.STRING
    },
    Year: {
        type: DataTypes.STRING
    },
    LicensePlate: {
        type: DataTypes.STRING
    },
    RFID: {
        type: DataTypes.STRING
    },
    CreatedAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        allowNull: false,
    },
    CreatedBy: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    ModifiedAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
    },
    ModifiedBy: {
        type: DataTypes.STRING
    }
},{
    timestamps: false,
    tableName: "JDVResidentVehicle"
});

module.exports = modelResidentVehicle;