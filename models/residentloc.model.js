const { Sequelize, DataTypes } = require("sequelize");
const sequelizeDB = require("../database/db");

const modelResidentLoc = sequelizeDB.define('jdvresidentloc', {
    ResidentLocId: {
        type: DataTypes.INTEGER, 
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    PersonId: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    Country: {
        type: DataTypes.STRING,
    },
    City: {
        type: DataTypes.STRING,
    },
    TypeOfHousing: {
        type: DataTypes.STRING
    },
    Street: {
        type: DataTypes.STRING
    },
     Block: {
        type: DataTypes.STRING
    },
    HouseNumber: {
        type: DataTypes.STRING
    },
    CreatedAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        allowNull: false,
    },
    CreatedBy: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    ModifiedAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
    },
    ModifiedBy: {
        type: DataTypes.STRING
    }
},{
    timestamps: false,
    tableName: "JDVResidentLoc"
});


module.exports = modelResidentLoc;