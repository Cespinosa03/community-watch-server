const { Sequelize, DataTypes } = require("sequelize");
const sequelizeDB = require("../database/db");
const modelRole = require("../models/role.model");

const modelUser = sequelizeDB.define('jdvuser', {
    UserId: {
        type: DataTypes.INTEGER, 
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    UserName: {
        type: DataTypes.STRING,
        unique: true,
        validate:{
            len: {
                args:[4,16],
                msg: "El Username debe contener 4 a 16 caracteres"
            }
        }
    },
    Password: {
        type: DataTypes.STRING,
        validate:{
            len: {
                args:[8,100],
                msg: "El Password debe contener 8 o mas caracteres" 
            }
        }  
    },
    PersonId: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
     RoleId: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    CreatedAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        allowNull: false,
    },
    CreatedBy: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    ModifiedAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
    },
    ModifiedBy: {
        type: DataTypes.STRING
    }
},{
    timestamps: false,
    tableName: "JDVUser"
});

 modelUser.belongsTo(modelRole, {
     foreignKey: 'RoleId'
 });

 modelRole.hasMany(modelUser, {
    foreignKey: 'RoleId'
})


module.exports = modelUser;