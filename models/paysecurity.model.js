const { Sequelize, DataTypes } = require("sequelize");
const sequelizeDB = require("../database/db");
const modelPerson = require('../models/person.model');

const modelPaySecurity = sequelizeDB.define('jdvresidentpayment', {
    ResidentPaymentId: {
        type: DataTypes.INTEGER, 
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    PersonId: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    Month: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    Pay: {
        type: DataTypes.STRING,
    },
    Debt: {
        type: DataTypes.STRING
    },
    Comment: {
        type: DataTypes.STRING
    },
    CreatedAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        allowNull: false,
    },
    CreatedBy: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    ModifiedAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
    },
    ModifiedBy: {
        type: DataTypes.STRING
    }
},{
    timestamps: false,
    tableName: "JDVResidentPayment"
});

// modelPerson.belongsTo(modelPerson,{
//     foreignKey: 'PersonId'
// });

module.exports = modelPaySecurity;