const { Sequelize, DataTypes } = require("sequelize");
const sequelizeDB = require("../database/db");
const modelUser = require('../models/user.model');
const modelLoc = require('../models/residentloc.model');
const modelVehicle = require('../models/residentvehicle.model');

const modelPerson = sequelizeDB.define('jdvperson', {
    PersonId: {
        type: DataTypes.INTEGER, 
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    FirstName: {
        type: DataTypes.STRING,
        allowNull: false,
        // validate: {
        //     isAlpha: true      
        // }
    },
        LastName: {
        type: DataTypes.STRING,
        allowNull: false,
        // validate: {
        //     isAlpha: true      
        // }
    },
    DocumentId: {
        type: DataTypes.STRING,
        unique: true,
    },
    PhoneNumber: {
        type: DataTypes.STRING
    },
    CelNumber: {
        type: DataTypes.STRING
    },
    Email: {
        type: DataTypes.STRING,
        unique: true,
        validate:{
            isEmail:{
                msg: "Formato de Email incorrecto incorrecto"
            }
        }
    },
    CreatedAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        allowNull: false,
    },
    CreatedBy: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    ModifiedAt: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
    },
    ModifiedBy: {
        type: DataTypes.STRING
    },
     IsActive: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
    }
},{
    timestamps: false,
    tableName: "JDVPerson"
});

modelPerson.belongsTo(modelUser,{
    foreignKey: 'PersonId'
});
modelPerson.hasMany(modelLoc,{
    foreignKey: 'PersonId'
});

modelPerson.belongsTo(modelVehicle,{
    foreignKey: 'PersonId'
});



module.exports = modelPerson;