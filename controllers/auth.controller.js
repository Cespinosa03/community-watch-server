const userModel = require('../models/user.model');
const RoleModel = require('../models/role.model');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const env = require('../utils/auth')

//funcion para crear usuarios
const signUp = async (req, res) => {

     try {
          const password = bcrypt.hashSync(req.body.password, Number.parseInt(env.AUTH_ROUNDS));

          const user = await  userModel.create({
               UserName: req.body.username.toUpperCase(),
               Password: password,
               PersonId: req.body.personid,
               RoleId: req.body.roleid,
               CreatedBy: req.body.createdby,
               ModifiedBy: req.body.modifiedby
          })     
               const token = jwt.sign({user: user}, env.AUTH_SECRET, {
                    expiresIn: env.AUTH_EXPIRES
               });

          res.json({
                    user: user,
                    token: token
          });
     } catch (err) {
          res.status(500).json(err);
     }
}


//funcion para traer el token del usuario y autentificarse
const signIn = async (req, res) => {

     try {
          const {username, password} = req.body;

          const user = await userModel.findOne({
               attributes:['Password'],
               where: {
                    UserName: username
               },
               include: {
                    model: RoleModel,
                    attributes: ['RoleName']
               }
          });

          const userJson = JSON.parse(JSON.stringify(user)); 
          delete userJson.Password

          if (!user) {
               res.status(404).json({message: "Nombre de usuario no encontrado"});
          } else {
                
               if (bcrypt.compareSync(password, user.Password)){
 
                    const token = jwt.sign({user: user}, env.AUTH_SECRET, {
                    expiresIn: env.AUTH_EXPIRES
                    });
                    
                    res.json({
                         user: userJson,
                         token: token
                    });

               } else {
                    res.status(401).json({message: "Contrasena incorrecta"});
               }
          };
          
     } catch (err)  {
          res.status(500).json(err);
     }
}


module.exports = {signUp, signIn};