const modelUser = require('../models/user.model');
const modelRole = require('../models/role.model');

//funcion para traer todos los usuarios 
const getUser = async (req, res) => {
    try {
        const users = await modelUser.findAll({
            include: {
                model: modelRole,
                attributes: ['RoleName']
            },
            attributes: ['UserName'],
            where: {
                
            }
        });
        res.json(users); 
    } catch (err) {
        res.status(500).json({message: 'Error en el servidor'});
    }
   
};

module.exports = {getUser};