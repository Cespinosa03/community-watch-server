const personModel = require('../models/person.model');
const userModel = require('../models/user.model');
const jwt = require('jsonwebtoken');
const env = require('../utils/auth');
const residentLoc = require('../models/residentloc.model');
const residentVehicle = require('../models/residentvehicle.model');
//funcion para crear personas
const createPerson = async (req, res) => {

     try {

          const user = await  personModel.create({
               FirstName: req.body.firstname,
               LastName: req.body.lastname,
               DocumentId: req.body.documentid,
               PhoneNumber: req.body.phonenumber,
               CelNumber: req.body.celnumber,
               Email: req.body.email.toUpperCase(),
               CreatedBy: req.body.createdby,
               ModifiedBy: req.body.modifiedby,
               IsActive: req.body.isactive
          })     
               const token = jwt.sign({user: user}, env.AUTH_SECRET, {
                    expiresIn: env.AUTH_EXPIRES
               });

          res.json({
                    user: user,
                    token: token
          });
     } catch (err) {
          res.status(500).json(err);
     }
}

//funcion para traer todas personas de la tabla person, que esten activo y el role sea null en la tabla de user
const getAllPerson = async (req, res) => {
     try {
          const persons = await personModel.findAll({
               attributes: ['PersonId','FirstName','LastName','DocumentId','PhoneNumber','CelNumber','Email'],
               where: {
                    IsActive: true
               }
          })
          res.json(persons)
     } catch (err) {
          res.status(500).json(err);     
     }
}

//funcion para traer todos los usuarios de la tabla person, que esten activo y el role sea null en la tabla de user
const getOnePerson = async (req, res) => {
     try {

          const {username} = req.body; 

          const persons = await personModel.findOne({
               include: {
                    model: userModel,
                    attributes: ['UserName'],
                    where:{
                         UserName: username
                    },
               },
               attributes: ['FirstName','LastName','DocumentId','PhoneNumber','CelNumber','Email'],
               where: {
                    IsActive: true
               }
          })
          res.json(persons)
     } catch (err) {
          res.status(500).json(err);     
     }
}

const locationPerson = async (req, res) => {
     try {

          const {personid} = req.body; 

          const location = await personModel.findOne({
               attributes: ['PersonId','FirstName','LastName','DocumentId','PhoneNumber','CelNumber','Email'],
                  where:{
                         PersonId: personid
                    },       
               include: {
                    model: residentLoc,
               },  
          })
          res.json(location)
     } catch (err) {
          res.status(500).json(err);     
     }
}
const vehiclePerson = async (req, res) => {
     try {

          const {personid} = req.body; 

          const persons = await personModel.findOne({

               include: {
                    model: residentVehicle,
                    where:{
                         PersonId: personid
                    },
               },
               attributes: ['FirstName','LastName','DocumentId','PhoneNumber','CelNumber','Email'],
          })
          res.json(persons)
     } catch (err) {
          res.status(500).json(err);     
     }
}


// const getPayPerson = async (req, res) => {

//     const id = req.body;
//      try {
//           const pay = await paySecurityModel.findAll({
//                attributes: ['ResidentPaymentId','PersonId','Month','Pay','Debt','Comment'],
//                where: {
//                     PersonId: id
//                }
//           })
//           res.json(pay)
//      } catch (err) {
//           res.status(500).json(err);     
//      }
// }


module.exports = {createPerson, getAllPerson,getOnePerson,locationPerson,vehiclePerson};
